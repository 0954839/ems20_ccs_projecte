/*
 * init_mcu.c
 *
 *  Created on: 25 apr. 2018
 *      Author: VersD
 */


/* --------------------- Alle Routines om Startup code te laten werken ----- */
#include "register_def.h"
#include "init_mcu.h"


void UtilsDelay(volatile unsigned long ulCount)
{
    while(ulCount--);
}

//*****************************************************************************
//
//! Reads 32-bit value from register at specified address
//!
//! \param ulRegAddr is the address of register to be read.
//!
//! This function reads 32-bit value from the register as specified by
//! \e ulRegAddr.
//!
//! \return Return the value of the register.
//
//*****************************************************************************
unsigned long PRCMHIBRegRead(unsigned long ulRegAddr)
{
  unsigned long ulValue;

  //
  // Read the Reg value
  //
  ulValue = HWREG(ulRegAddr);

  //
  // Wait for 200 uSec
  //
  UtilsDelay((80*200)/3);

  //
  // Return the value
  //
  return ulValue;
}

//*****************************************************************************
//
//! Writes 32-bit value to register at specified address
//!
//! \param ulRegAddr is the address of register to be read.
//! \param ulValue is the 32-bit value to be written.
//!
//! This function writes 32-bit value passed as \e ulValue to the register as
//! specified by \e ulRegAddr
//!
//! \return None
//
//*****************************************************************************
void PRCMHIBRegWrite(unsigned long ulRegAddr, unsigned long ulValue)
{
  //
  // Read the Reg value
  //
  HWREG(ulRegAddr) = ulValue;

  //
  // Wait for 200 uSec
  //
  UtilsDelay((80*200)/3);
}

//init routine
void initMCU(void)
{

    unsigned long ulRegValue;

    //
    // DIG DCDC LPDS ECO Enable
    //
    HWREG(0x4402F064) |= 0x800000;

    //
    // Enable hibernate ECO for PG 1.32 devices only. With this ECO enabled,
    // any hibernate wakeup source will be kept maked until the device enters
    // hibernate completely (analog + digital)
    //
    ulRegValue = PRCMHIBRegRead(HIB3P3_BASE  + HIB3P3_O_MEM_HIB_REG0);
    PRCMHIBRegWrite(HIB3P3_BASE + HIB3P3_O_MEM_HIB_REG0, ulRegValue | (1<<4));

    //
    // Handling the clock switching (for 1.32 only)
    //
    HWREG(0x4402E16C) |= 0x3C;

        //
    // Enable uDMA
    //
    //PRCMPeripheralClkEnable(PRCM_UDMA,PRCM_RUN_MODE_CLK);
    HWREG(ARCM_BASE + APPS_RCM_O_UDMA_A_CLK_GATING ) |= APPS_RCM_UDMA_A_CLK_GATING_UDMA_A_RUN_CLK_ENABLE;

    //
    // Reset uDMA
    //
    //PRCMPeripheralReset(PRCM_UDMA);
    HWREG(ARCM_BASE + APPS_RCM_O_UDMA_A_SOFT_RESET ) |= APPS_RCM_UDMA_A_SOFT_RESET_UDMA_A_SOFT_RESET;

    //
    // Disable uDMA
    //
    HWREG(ARCM_BASE + APPS_RCM_O_UDMA_A_CLK_GATING ) &= ~APPS_RCM_UDMA_A_CLK_GATING_UDMA_A_RUN_CLK_ENABLE;

    //
    // Enable RTC
    //
    PRCMHIBRegWrite(0x4402F804,0x1);

      //
    // Override JTAG mux
    //
    HWREG(0x4402E184) |= 0x2;

    //
    // DIG DCDC VOUT trim settings based on PROCESS INDICATOR
    //
    if(((HWREG(0x4402DC78) >> 22) & 0xF) == 0xE)
    {
      HWREG(0x4402F0B0) = ((HWREG(0x4402F0B0) & ~(0x00FC0000))|(0x32 << 18));
    }
    else
    {
      HWREG(0x4402F0B0) = ((HWREG(0x4402F0B0) & ~(0x00FC0000))|(0x29 << 18));
    }

    //
    // Enable SOFT RESTART in case of DIG DCDC collapse
    //
    HWREG(0x4402FC74) &= ~(0x10000000);

    //
    // Enable NWP force reset and HIB on WDT reset
    // Enable direct boot path for flash
    //
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_SPARE_REG_8) |= ((7<<5) | 0x1);
    if((HWREG(HIB3P3_BASE + HIB3P3_O_MEM_HIB_REG2) & 0x1) )
    {
        HWREG(HIB3P3_BASE + HIB3P3_O_MEM_HIB_REG2) &= ~0x1;
        HWREG(OCP_SHARED_BASE + OCP_SHARED_O_SPARE_REG_8) |= (1<<9);

        //
        // Clear the RTC hib wake up source
        //
        HWREG(HIB3P3_BASE+HIB3P3_O_MEM_HIB_RTC_WAKE_EN) &= ~0x1;

        //
        // Reset RTC match value
        //
        HWREG(HIB3P3_BASE + HIB3P3_O_MEM_HIB_RTC_WAKE_LSW_CONF) = 0;
        HWREG(HIB3P3_BASE + HIB3P3_O_MEM_HIB_RTC_WAKE_MSW_CONF) = 0;

    }

    unsigned long efuse_reg2;
    unsigned long ulDevMajorVer, ulDevMinorVer;
    //
    // Read the device identification register
    //
    efuse_reg2= HWREG(GPRCM_BASE + GPRCM_O_GPRCM_EFUSE_READ_REG2);

    //
    // Read the ROM mojor and minor version
    //
    ulDevMajorVer = ((efuse_reg2 >> 28) & 0xF);
    ulDevMinorVer = ((efuse_reg2 >> 24) & 0xF);

    if(((ulDevMajorVer == 0x3) && (ulDevMinorVer == 0)) || (ulDevMajorVer < 0x3))
    {
        unsigned int Scratch, PreRegulatedMode;

        // 0x4402F840 => 6th bit indicates device is in pre-regulated mode.
        PreRegulatedMode = (HWREG(0x4402F840) >> 6) & 1;

        if( PreRegulatedMode)
        {
          Scratch = HWREG(0x4402F028);
          Scratch &= 0xFFFFFF7F; // <7:7> = 0
          HWREG(0x4402F028) = Scratch;

          Scratch = HWREG(0x4402F010);
          Scratch &= 0x0FFFFFFF; // <31:28> = 0
          Scratch |= 0x10000000; // <31:28> = 1
          HWREG(0x4402F010) = Scratch;
        }
        else
        {
          Scratch = HWREG(0x4402F024);

          Scratch &= 0xFFFFFFF0; // <3:0> = 0
          Scratch |= 0x00000001; // <3:0> = 1
          Scratch &= 0xFFFFF0FF; // <11:8> = 0000
          Scratch |= 0x00000500; // <11:8> = 0101
          Scratch &= 0xFFFE7FFF; // <16:15> = 0000
          Scratch |= 0x00010000; // <16:15> = 10

          HWREG(0x4402F024) = Scratch;

          Scratch = HWREG(0x4402F028);

          Scratch &= 0xFFFFFF7F; // <7:7> = 0
          Scratch &= 0x0FFFFFFF; // <31:28> = 0
          Scratch &= 0xFF0FFFFF; // <23:20> = 0
          Scratch |= 0x00300000; // <23:20> = 0011
          Scratch &= 0xFFF0FFFF; // <19:16> = 0
          Scratch |= 0x00030000; // <19:16> = 0011

          HWREG(0x4402F028) = Scratch;
          HWREG(0x4402F010) &= 0x0FFFFFFF; // <31:28> = 0
        }
    }
    else
    {
        unsigned int Scratch, PreRegulatedMode;

        // 0x4402F840 => 6th bit indicates device is in pre-regulated mode.
        PreRegulatedMode = (HWREG(0x4402F840) >> 6) & 1;

        Scratch = HWREG(0x4402F028);
        Scratch &= 0xFFFFFF7F; // <7:7> = 0
        HWREG(0x4402F028) = Scratch;

        HWREG(0x4402F010) &= 0x0FFFFFFF; // <31:28> = 0
        if( PreRegulatedMode)
        {
          HWREG(0x4402F010) |= 0x10000000; // <31:28> = 1
        }
    }
}


